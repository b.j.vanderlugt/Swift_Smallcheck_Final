//
//  getFive.swift
//  SmallSwift
//
//  Created by Lukas Arts on 19/12/2017.
//  Copyright © 2017 Swiftyboys Inc. All rights reserved.
//
//  This is an example file containing:
//  - An actual user-defined function that was validated using Haskell quickcheck, ported to Swift
//	- Two properties that should hold for every output of this function
//	- The method performing the actual check

import Foundation

// import smallcheck

// The getFive method we want to validate
// Source of thi usecase: https://wiki.haskell.org/Introduction_to_QuickCheck2
// This recursive function should return the first five occurences of characters a...e from a string
func getFive(_ str : String , _ n : Int = 5) -> String{
	if(n == 0 || str == "") {return ""}
	else{
		let ch = String(Array(str)[0]);
		if(["a","b","c","d","e"].contains(ch)){
			let tl = getFive(String(str.dropFirst()),n-1)
			return ch+tl
		}
		else{
			return getFive(String(str.dropFirst()),n)
		}
	}
}

// The first getFive invariant defined as a property: the function should always return a string with length smaller than 5
func getFive_property(_ str : String)->Bool{
	return getFive(str,5).count <= 5
}

// The second getFive invariant defined as a property: the function should always return a string that only contains the characters a...e.
func getFive_2nd_property(_ str : String)->Bool{
	for ch in getFive(str,5){
		if(!(["a","b","c","d","e"].contains(ch))) {return false}
	}
	return true
}

// Function performing the actual check of the second getFive property for strings of depth 5.
print(depthCheck(getFive_2nd_property, 5))

