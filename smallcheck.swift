﻿//
//  smallcheck.swift
//  SmallSwift
//
//  Created by Lukas Arts on 19/12/2017.
//  Copyright © 2017 Swiftyboys Inc. All rights reserved.
//
//  https://hackage.haskell.org/package/smallcheck-0.4/src/Test/SmallCheck.hs
//
//  This file contains all the structures and functions required to perform basic checks of functions.
//  All methods and structures are based on the Smallcheck 0.4 Haskell source mentioned above.
//  We implement:
//	- Extensions for all basic data types, allowing easy construction of lists of all possible values.
//	- Addition and Cross product operator for lists of possible values.
//	- A checking function, that only needs a user-defined property and returns cases up to a certain depth 			that do not sattisfy the input property

import Foundation

typealias Series<A> = (Int) -> ([A])

infix operator ++:AdditionPrecedence
//Sum operator
func &&<A>(left: @escaping Series<A>, right: @escaping Series<A>) -> Series<A> { // 1
    return { (d: Int) -> [A] in
        left(d)+right(d)
    };
}

infix operator ><:AdditionPrecedence
//Product operator: returns the cross product of two series of any two types up to a certain depth.
func ><<A,B>(left: @escaping Series<A>, right: @escaping Series<B>) -> Series<(A,B)> { // 1
    return { (d: Int) -> [(A,B)] in
        var r: [(A,B)] = [];

        for x in left(d) {
            for y in right(d) {
                r.append((x,y))
            }
        }
        return r
    };
}

// The blueprint for our Serial protocol which we will use to extend all our base classes.
// Types conforming to this protocol should implement a function taking an input depth and returning an     array of elements with increasing complexity/size depending on the depth.
protocol Serial {
    static func series(_ d: Int) -> [Self]
}

// Returns a list of all integers between -depth and depth
extension Int : Serial {
    static func series(_ d: Int) -> [Int] {
        if(d>=0) {
            return Array(-d...d)
        } else {
            return [0]
        }
    }
    static func next(_ d: Int) -> [Int] {
        return [-d,d]
    }
}

// Compute the Float series as a list.
// This is defined as all possible fractions that can possibly be made with integers of depth equal to the input depth
extension Float : Serial {
    static func series(_ d: Int) -> [Float]{
        var result : [Float] = []
        for sig in Int.series(d){
            for exp in Int.series(d) {
                if (sig % 2 != 0 || sig == 0 && exp == 0){
                    result.append(Float(sig) * pow(2.0, Float(exp)) )
                }
            }
        }
        return result
    }
    static func next(_ d: Int) -> [Float] {
        var result : [Float] = []

        for sig in Int.next(d) {
            for exp in Int.series(d) {
                if (sig % 2 != 0 || sig == 0 && exp == 0){
                    result.append(Float(sig) * pow(2.0, Float(exp)) )
                }
            }
        }
        for exp in Int.next(d){
            for sig in Int.series(d) {
                if (sig % 2 != 0 || sig == 0 && exp == 0){
                    result.append(Float(sig) * pow(2.0, Float(exp)) )
                }
            }
        }
        return result
    }
}

// Return the Double series as a list , using the float series operator as its backend
extension Double : Serial {
    static func series(_ d: Int) -> [Double] {
        return Float.series(d).map { Double($0) };
    }
}

// Return a list containing the first (depth) + 1 characters of the alphabet
extension Character : Serial {
    static func series(_ d: Int) -> [Character] {
        return Array("abcdefghijklmnopqrstuvwxyz".prefix(d+1))
    }
}

// Construct all possible combinations of (depth) characters a...z
extension String : Serial {
	static func series(_ d: Int) -> [String]{
		let alphabet = Character.series(26)
		var res = [""];	var temp = [""];

		//Initiate depth 0.
		for charB in alphabet{
			res.append(String(charB))
		}
		//Drop empty string
		res = Array(res.dropFirst())

		//Take the cross product of the 'alphabet' with our current collection of strings to add 1 depth.
		if(d > 0){
			var curr_depth = res;
			for _ in 1...d {
				for stringA in curr_depth{
					for charB in alphabet{
						temp.append(stringA + String(charB))
					}
				}
				temp = Array(temp.dropFirst())
				curr_depth = temp;
				res = res + temp;
			}
		}
		return res;
	}

}

// A custom tuple struct implementing all features of a regular two-type struct.
// This was used because tuples can not be extended with protocols in Swift
struct myTuple<A:Serial,B:Serial> : CustomStringConvertible {
    var var1 : A
    var var2 : B

    init(v1:A,v2:B) {
        var1 = v1;
        var2 = v2;
    }

	var description : String{
		return "{\(var1), \(var2)}"
	}
}

// The serial extension implemented for our custom tuple struct.
// Returns the cross product of the two type series from the types contained in our tuple.
extension myTuple : Serial {
    static func series(_ d: Int) -> [myTuple] {
        let res = (A.series >< B.series)(d)
		var tuple : [myTuple] = []
		for resp in res{
			tuple.append(myTuple(v1:resp.0, v2:resp.1))
		}
		return tuple;
    }
}

// The checking function performing the check for iteratively increasing input depth.
// Prints the first example that returns false.
// Note: this function was intended for use with the next() operator, but this was not yet defined for every datatype
func check<A:Serial>(_ property: (A)->Bool, _ maxdepth: Int) -> Bool {
    for i in 0...maxdepth {
        for ca in A.series(i) {
            if(!property(ca)) {
                print(ca);
                return false
            }
        }
    }
    return true
}

// The checking function for given depth. Prints all examples not satisfying the property.
// It does not require a type specification, just a well-defined property.
func depthCheck<A:Serial>(_ property: (A)->Bool, _ depth: Int) -> Bool {
	var res : Bool = true;
   	var number : Int = 0;
    	var numbererror : Int = 0;
    
        for ca in A.series(depth) {
            number = number+1;
            if(!property(ca)) {
                print(ca);
                res = false;
                numbererror = numbererror+1;
                //return res
            }
        }
    print(number);
    print(numbererror);
    return res
}