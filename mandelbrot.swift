//
//  mandelbrot.swift
//  SmallSwift
//
//  Created by Lukas Arts on 19/12/2017.
//  Copyright © 2017 Swiftyboys Inc. All rights reserved.
//
//  This is an example file containing:
//  - A user function (mandelbrotIter), which calculates the number of iterations of the mandelbrot function
//      on a point, before it diverges from a circle of fixed radius.
//  - Properties defining a threshold for which we want to check the number of iterations
//  - The method actually performing the check, using the smallcheck.swift library
//  - Prints of some exmaple points we found when we deployed the algorithm ourselves
import Foundation

// import smallcheck

// Iteratively calculate the mandelbrot number from a given starting point,
// See e.g. https://en.wikipedia.org/wiki/Mandelbrot_set,
// and for our real-valued implementation: http://blog.scottlogic.com/2014/10/29/concurrent-functional-swift.html
//
// Returns the amount of iterations required to reach a point with distance greater than 5 from the origin.
// Or return the maximum amount of iterations (100) to prevent buffer overflow.
func mandelbrotIter(_ cx: Double, _ cy: Double) -> Int{
  let bounds = 5.0
  var iteration : Int = 0
  let maxIter : Int = 100
  var zx = 0.0
  var zy = 0.0
  while (zx * zx + zy * zy < bounds && iteration < maxIter) {
    let tmp = zx * zx - zy * zy + cx
    zy = 2.0 * zx * zy + cy
    zx = tmp
    iteration = iteration + 1
  }
  return iteration
}

// The property we wish to check using our SmallCheck library.
// Returns true when a point requires less than 100 iterations to diverge.
func mandelbrot_property(_ T : myTuple<Double, Double>) -> Bool{
	return mandelbrotIter(T.var1,T.var2) < 99
}

// Perform the check, printing points that do not diverge within the maximum amount of iterations.
print(depthCheck(mandelbrot_property, 5))

// One of the points that doesnt diverge within 100 iterations
print(mandelbrotIter(-0.15625,0.75))

// Two of the points that do diverge within 100 iterations
print(mandelbrotIter(-0.75, -0.0625))	// prints "51"
print(mandelbrotIter(0.625, 0.09375))	// prints "4"
