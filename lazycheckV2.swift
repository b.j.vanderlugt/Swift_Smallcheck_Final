//
//  LazyCheckV2.swift
//  SmallSwift
//
//  Copyright © 2017 Swiftyboys Inc. All rights reserved.
//

///////////////////////////////////////////////////
// For usage, see on the bottom of this file
///////////////////////////////////////////////////

///////////////////////////
//Helper functions for Swift
///////////////////////////

extension Array{

	var head: Element?{
		return self.first
	}
	
	var tail: Array?{
		if self.isEmpty {
			return nil
		}
		return Array(self.dropFirst())
	}
}

//Define the constant function. It takes two arguments, but always returns the first.
func const<A, B>(_ first : A) -> ((B) -> (A)){
	return { (b: B) -> A in
		first
	}
}

//Just like the haskell function splitAt
func splitAt<A>(_ n: Int, _ xs: Array<A>) -> (Array<A>, Array<A>){
	let length = xs.count
	var m = n
	if n > length {
		m = length }
	if n < 0 {
		m = 0 }
	
	return (Array(xs.dropLast(length - m)), Array(xs.dropFirst(m)))
}


///////////////////////////////////
//Types and stuff
///////////////////////////////////
//This is how we indicate the position of the error, in the data structure.
typealias Pos = [Int]

enum UnevaluatedError : Error{
	case notYetEvaluated(Pos)
}

protocol Partially {
	var errorPos : Pos? {get}
	var isPartially : Bool {get}
}

protocol BaseCase {
	static var base : Self {get}
}

//Since we cannot store additional information in existing types, we come up with a wrapper structure where we store the additional information needed.
struct Almost<A: BaseCase> : Partially {
	var value : A
	var SetIsPartially : Bool
	var SetErrorPos : Pos?
	var errorPos : Pos? { return SetErrorPos }
	var isPartially : Bool { return SetIsPartially}
	
	init(){
		self.value = A.base
		self.SetIsPartially = false
		self.SetErrorPos = nil
	}
	
	init(_ v : A){
		self.init()
		self.value = v
	}
	
	init(_ p: Pos){
		self.init()
		self.SetIsPartially = true
		self.SetErrorPos = p
	}
}

extension Int : BaseCase {
	static var base = 0
}

extension Bool : BaseCase {
	static var base = false
}

//To pretty print our type
extension Almost : CustomStringConvertible {
	var description : String {
		if self.isPartially {
			return "_|_"
		} else {
		return "\(value)"
		}
	}
}

/////////////////////////////////////////////////////////////////
/// Here all operators are defined that can be used in the functions
/////////////////////////////////////////////////////////////////
// This checks if it's argument is undefined and throws an error if it is.
func partialCheck(_ x : Partially) throws -> (){
	guard !x.isPartially else {
		throw UnevaluatedError.notYetEvaluated(x.errorPos!)
	}
}

// Almost all operators work the same, so we wrote a generic function for them.
func opGen<A, B>(left: Almost<A>, right: Almost<A>, op: (A,A) -> B) throws ->  Almost<B>{
	try partialCheck(left)
	try partialCheck(right)
	
	let result = op(left.value, right.value)
	
	return Almost(result)
}

//Except the && and || operator, we want them to work lazily
func &&(left: Almost<Bool>, right: Almost<Bool>) throws -> Almost<Bool>{
	try partialCheck(left)
	if !left.value {
		return Almost(false)
	}
	
	try partialCheck(right)
	return right
}

func ||(left: Almost<Bool>, right: Almost<Bool>) throws -> Almost<Bool>{
	try partialCheck(left)
	if left.value {
		return Almost(true)
	}
	
	try partialCheck(right)
	return right
}

func +<A: Numeric>(left: Almost<A>, right: Almost<A>) throws -> Almost<A>{
	return try opGen(left: left, right: right, op: (+))
}

func -<A: Numeric>(left: Almost<A>, right: Almost<A>) throws -> Almost<A>{
	return try opGen(left: left, right: right, op: (-))
}

prefix func -(left: Almost<Int>) throws -> Almost<Int>{
	try partialCheck(left)
	return Almost(-left.value)
}

func *<A: Numeric>(left: Almost<A>, right: Almost<A>) throws -> Almost<A>{
	return try opGen(left: left, right: right, op: (*))
}

func /(left: Almost<Int>, right: Almost<Int>) throws -> Almost<Int>{
	return try opGen(left: left, right: right, op: (/))
}

func ==<A: Equatable>(left: Almost<A>, right: Almost<A>) throws -> Almost<Bool>{
	return try opGen(left: left, right: right, op: (==))
}

func !=<A: Equatable>(left: Almost<A>, right: Almost<A>) throws -> Almost<Bool>{
	return try opGen(left: left, right: right, op: (!=))
}

func >=<A: Comparable>(left: Almost<A>, right: Almost<A>) throws -> Almost<Bool>{
	return try opGen(left: left, right: right, op: (>=))
}

func ><A: Comparable>(left: Almost<A>, right: Almost<A>) throws -> Almost<Bool>{
	return try opGen(left: left, right: right, op: (>))
}

func <=<A: Comparable>(left: Almost<A>, right: Almost<A>) throws -> Almost<Bool>{
	return try opGen(left: left, right: right, op: (<=))
}

func <<A: Comparable>(left: Almost<A>, right: Almost<A>) throws -> Almost<Bool>{
	return try opGen(left: left, right: right, op: (<))
}

prefix func !(left: Almost<Bool>) throws -> Almost<Bool>{
	try partialCheck(left)
	return Almost(!left.value)
}

//it is right associative, and we want it just higher then assigment, this gives us TernaryPrecedence
infix operator ==>: TernaryPrecedence
func ==>(left:Almost<Bool>, right: Almost<Bool>) throws -> Almost<Bool>{
	try partialCheck(left)
	if !left.value {
		return Almost(true)
	}
	
	try partialCheck(right)
	return right
}

//////////////////////////////////
// Our Algebraic data structure
//////////////////////////////////
indirect enum List<A>{
//This might seem a bit strange, but we store if it is uninitialized, in the Empty constructor. This is because we don't really have any other choice
	case Empty(Pos?)
	case Cont(A, List<A>)
	
	func head() -> A? {
		switch self {
			case .Empty : return nil
			case let .Cont(i, _) : return i
		}
	}
	
	func tail() -> List? {
		switch self {
			case .Empty : return nil
			case let .Cont(_, iss) : return iss
		}
	}
}

extension List : Partially {
	var errorPos : Pos? {
		switch self {
			case let .Empty(p) : return p
			case .Cont(_, _) : return nil
		}
	}
	
	var isPartially : Bool {
		switch self {
			case let .Empty(p) : if p != nil { return true } else { return false}
			case .Cont(_, _) : return false
		}
	}
}

extension List : BaseCase {
	static var base : List { return .Empty(nil) }
}


func helper<A>(_ l : List<A>) -> String{
	switch l {
			case let .Empty(p) : if p != nil { return "_|_" }
			else { return "[]"}
			case let .Cont(x, xs) : return "\(x) :" + helper(xs)
		}
}

extension List : CustomStringConvertible {
	var description : String {
		return helper(self)
	}
}


typealias AInt = Almost<Int>
typealias ABool = Almost<Bool>

////////////////////////////////
//Creation of series
////////////////////////////////

protocol Series : Partially {
	func series(_ depth : Int) -> [Self]
}

extension Almost : Series {
	func series(_ depth : Int) -> [Almost]{
		return [Almost([0])]
	}
}

extension List : Series {
	func series(_ depth : Int) -> [List]{
		if depth <= 0 { return [.Empty(nil)] }
		return [.Empty([])]
	}
}

func refuteA(_ partial : Almost<Int>, _ depth : Int) -> [Almost<Int>]{
	var result : [Almost<Int>] = []
	for i in -depth...depth {
		result.append(Almost(i))
	}
	return result
}

func refuteL(_ partial : List<AInt>, _ depth : Int, _ pos : Pos) -> [List<AInt>]{
	let newdepth = max(depth - pos.count, 0)
	if pos.isEmpty{
		if newdepth == 0 {
			return [.Empty(nil)]
		}
		return [.Empty(nil), .Cont(Almost([0]), .Empty([1]))]
	
	}
	let x = pos.last!
	if x == 0 {
		var result : [List<AInt>] = []
		for i in -newdepth...newdepth {
			result.append(replaceV(partial, Almost(i), pos))
		}
		
		return result
	} else {
		if newdepth == 0 {
			return [replaceL(partial, .Empty(nil), pos)]
		} else {
			return [replaceL(partial, .Empty(nil), pos),
					replaceL(partial, .Cont(Almost(pos + [0]), .Empty(pos + [1])), pos)
					]
		}
	}
}

func replaceL(_ original : List<AInt>, _ element : List<AInt>, _ pos : Pos) -> List<AInt>{
	if pos.isEmpty {
		return element
	} else {
		switch original {
		//The empty case should never be reached
			case .Empty(_) : fatalError("The Empty case should never be reached in replaceL")
			case let .Cont(i, iss) :
				return .Cont(i, replaceL(iss, element, pos.tail!))
		}
	}
}

func replaceV(_ original : List<AInt>, _ element : AInt, _ pos : Pos) -> List<AInt>{
	if pos.head! == 0 {
		switch original {
		//The empty case should never be reached
			case .Empty(_) : fatalError("The Empty case should never be reached in replaceV")
			case let .Cont(_, iss) :
				return .Cont(element, iss)
		}
	} else {
		switch original {
			//The empty case should never be reached
			case .Empty(_) : fatalError("The Empty case should never be reached in replaceV")
			case let .Cont(i, iss) :
				return .Cont(i, replaceV(iss, element, pos.tail!))
		}
	}
}

////////////////////////////////
//Check function
////////////////////////////////
func Checker(_ f : ((List<AInt>) throws ->  ABool), _ depth : Int, _ showInputs : Bool = false){
	print("We start checking the function")
	for i in 0...depth {
		print("Check at depth \(i):")
		let base : List<AInt> = .Empty([])
		let (tests2, result2) = CheckerHelper(f, i, base, 0, showInputs)
		if result2 {
			print("Completed with \(tests2) tests for this depth")
		} else {
			break
		}
	}

}

func CheckerHelper(_ f : ((List<AInt>) throws ->  ABool), _ depth : Int, _ input :List<AInt>, _ tests : Int, _ showInputs : Bool) -> (Int, Bool){
	var newtests = tests + 1
	
	if showInputs {
	print(input)
	}
	
	do {
		let result = try f(input)
		if !result.value {
			print("Counter example found after \(tests) tests:")
			print("\(input)")
			return (newtests,false)
		} else {
			return (newtests,true)
		}
	} catch UnevaluatedError.notYetEvaluated(let p) {
		let newinputs = refuteL(input, depth, p)
		for news in newinputs {
			let (tests2, result2) = CheckerHelper(f, depth, news, newtests, showInputs)
			newtests = tests2
			if !result2 {
				return (newtests, false)
			}
		}
		return (newtests,true)
	
	} catch {
		fatalError("Wow this is another error, ouch")
	}

}

func isSorted(_ list: List<AInt>) throws -> ABool{
	try partialCheck(list)
	switch list {
		case .Empty : return Almost(true)
		case let .Cont(i, iss) :
			try partialCheck(iss)
			switch iss {
				case .Empty : return Almost(true)
				case let .Cont(j, _) :
					let tresult = try j >= i
					if !tresult.value { return Almost(false) }
					return try isSorted(iss)
			}
	}
}


//////////////////////////
// Usage
//////////////////////////

//Use the function as following
// first argument: a function that has a List<AInt> as argument and returns a bool and can throw errors
//The function that you give as argument, should be build with the operators that are overloaded below (otherwise it will not throw (correct) errors)
// second argument: the depth up to which should be checked (an Int)
// third argument (optional): a bool, that indicates if the inputs that are tested should be printed on screen

Checker(isSorted, 4, true)

Checker({ (i) throws ->  ABool in try isSorted(i) ==> isSorted(i)}, 4)
