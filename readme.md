# (Lazy) SmallCheck Swift library

The code hosted here contains all our project files for the MCPD project. It features an implementation of a basic version of the Haskell SmallCheck library in Swift. It also features our partial implementation of Lazy SmallCheck ported to Swift. 
Source: http://www.cs.uu.nl/docs/vakken/mcpd/projects.html

## Smallcheck.swift

This file contains the main library code. It implements the Serial extension for all basic data types, includes an operator to perform addition and cross product operations on series, and implements a checking function that takes a property of a program and a depth as input, automatically validating the property for all possible values of the specified type up to the given depth.

## Example files

We have written 3 example code files illustrating different cases of the practical use of SmallCheck. These files contain a function defined by a user, a property of this function that the user wishes to validate, and the actual method performing the validation of the property and printing results.

Depending on the version of XCode you are using, you may of may not have to include the checking library in the example code files. This can be done by placing both the example files in the same project directory and compiling the example file using a recent version of XCode. If that does not work, one must include the line

```
include smallcheck.swift
```

At the top of the example file in order for the depthCheck function to be ready for use.

## LazySmallcheck.swift (V1 and V2)

These files contain our efforts in trying to port the lazy smallcheck library from Haskell to Swift. Due to missing features in the functional portion of the Swift programming language, we did not completely succeed in these efforts. An initial check using overloaded data types and functors can be performed. Both versions of the code are included to give a maximum amount of insight in our thought- and development process.

## Usege of the checking function

An initial proof-of-concept is provided in the V2 version of the library, in the form of a very simple check. One must provide some very specific information to the checking function in order to get results. 
 - First argument: a function that has a List<AInt> as argument and returns a bool and can throw errors
The function that you give as argument, should be built with the operators that are overloaded in our code. (Otherwise it will not throw the (correct) errors)
 - Second argument: the depth up to which should be checked (an Int)
 - Third argument (optional): a bool, that indicates if the inputs that are tested should be printed on screen

For example, because we have provided an implementation ofthe isSorted function using overloaded data types and functors, one can call:

```
Checker(isSorted, 4, true)
```
 

## Authors

* **Lars van den Haak**
* **Lukas Arts**
* **Bart van der Lugt**
* **Roald Neuteboom**
* **Anvar Arashov**

## Acknowledgments

* Wouter Swierstra for helpful assistance during practicum sessions.
* http://matt.might.net/articles/quick-quickcheck/
* https://wiki.haskell.org/Introduction_to_QuickCheck2