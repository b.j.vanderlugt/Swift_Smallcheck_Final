//
//  collatz.swift
//  SmallSwift
//
//  Created by Lukas Arts on 19/12/2017.
//  Copyright © 2017 Swiftyboys Inc. All rights reserved.
//
//  This is an example file containing:
//  - a user defined function, iteratively computing the result of the function featured

import Foundation

// import smallcheck

// The arithmetic function featured in the Collatz conjecture
// Source: https://en.wikipedia.org/wiki/Collatz_conjecture
// Source of our usecase: http://matt.might.net/articles/quick-quickcheck/
// Collatz conjecture sais that the series computed by iteratively applying this function
// from any starting integer converges to 1.
func collatz(_ n : Int) -> Int{
	if(n == 1) { return 1; }
	if(n % 2 == 0) { return collatz(n/2) }
	else { return collatz(3*n + 1) }
}

// The property we wish to validate using our checking algorithm;
//	does the collatz conjecture hold for every positive integer in our input set?
func collatz_property(_ n : Int)->Bool{
	if(n < 1) {return true}
	else{ return collatz(n) == 1 }
}

// Function performing the actual check
print(depthCheck(collatz_property, 5))