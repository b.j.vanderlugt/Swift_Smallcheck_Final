//
//  lazycheck.swift
//  SmallSwift
//
//  Created by Lars van den Haak on 14/12/2017.
//  Copyright © 2017 Swiftyboys Inc. All rights reserved.
//
// Ported from Lazy SmallCheck library version 0.2
// https://hackage.haskell.org/package/lazysmallcheck-0.2/docs/src/Test-LazySmallCheck.html#Serial 

typealias Pos = [Int]

indirect enum Type{
	case SumOfProd([[Type]])
}

indirect enum Term {
	case Var(Pos, Type)
	case Ctr(Int, [Term])
}

indirect enum Cons<A> {
	case C(Type, [([Term]) -> A])
}

typealias Series<A> = (Int) -> (Cons<A>)

protocol Serial {
    static func series() -> Series<Self>
}

////////////////////////////
// Series constructors
////////////////////////////

func cons<A>(_ a : A) -> Series<A>{
	return { (d: Int) -> Cons<A> in
		Cons<A>.C(Type.SumOfProd([[]]), [const(a)])
	}
}

//The (><) operator
func appl<A : SerialError, B>(_ f : @escaping Series<((A) -> B)>, _ a : @escaping Series<A>) -> Series<B>{
	return { (d: Int) -> Cons<B> in
		let t1 : Type
		let ps : [[Type]]
		let cfs : [([Term]) -> ((A) -> B)]
		let ta : Type
		let cas : [([Term]) -> A]
		var cs : [([Term]) -> B] = []
		var resultType : [[Type]] = []
		
		
		(t1, cfs) = getC( f(d) )
		ps = getSumOfProd(t1)
		
		(ta, cas) = getC( a(d-1))
		
		if d > 0 {
			for cf in cfs {
				cs.append(
					{ (xxs: [Term]) -> B in
						let x = xxs.head!
						let xs = xxs.tail!
						let tempf = cf(xs)
						return tempf(conv(cas, x) )
					}
				)
			}
			
			for p in ps {
				resultType.append(
				[ta] + p
				)
			}
		}
		
		return .C(.SumOfProd(resultType), cs)
		
	}
}

// the (\/) operator

func appl1<A>(_ a: @escaping Series<A>, _ b: @escaping Series<A> ) -> Series<A>{
    return { (d: Int) -> Cons<A> in
        let ssa : [[Type]]
        let ssb : [[Type]]
        let ca : [([Term]) -> A]
        let cb : [([Term]) -> A]
        
        
        let left_1 = a(d)
        switch left_1 {
        case let .C(t1, g1):
            ca = g1
            
            switch t1 {
            case let .SumOfProd(ts):
                ssa = ts
            }
        }
        
        
        let right_1 = b(d)
        switch right_1 {
        case let .C(t2, g2):
            cb = g2
            
            switch t2 {
            case let .SumOfProd(ts_1):
                ssb = ts_1
            }
        }
        let sss = ssa + ssb // concat
        let cc = ca + cb
        return Cons<A>.C(Type.SumOfProd(sss),cc)
    }
}


// Because error handling is different in swift (not lazy), we define a different protocol, where we store our error. Because if there is an error, we still have to give an value to be used (since not lazy), every type should have a base case. 
protocol SerialError {
	static var base : Self { get}
	var errorPos : Pos? {get set}
}

func conv<A : SerialError>(_ cs : [([Term]) -> A], _ t : Term) -> A{
	switch t {
			
		case let .Var(p, _):
			var dummy = A.base
			dummy.errorPos = p
			return dummy
		
		case let .Ctr(i, xs):
			return cs[i](xs)
	}
	
}

/////////////////////
// Term refinement
/////////////////////

func refine(_ term: Term, _ pos: Pos) -> [Term]{
	
	switch term{
		case let .Var(p, sopSS):
			let ss = getSumOfProd(sopSS)
			return new(p,ss)
			
		case let .Ctr(c, xs):
			return refineList(xs, pos).map { .Ctr(c, $0) }
	}
}


func refineList(_ xs: [Term], _ jss: Pos) -> [[Term]]{
	let ls : [Term]
	let rss : [Term]
	
	let j = jss.head!
	let js = jss.tail!
	
	
	(ls, rss) = splitAt(j, xs)
	let x = rss.head!
	let rs = rss.tail!
	
	return refine(x, js).map { ls + [$0] + rs}
}

func new(_ p: Pos, _ ps: [[Type]]) -> [Term]{	
	var result : [Term] = []
	var partresult : [Term]
	
	for (c, ts) in zip( 0...Int.max, ps) {
		partresult = []
				for (i, t) in zip ( 0...Int.max, ts){
					partresult.append( .Var( p + [i], t))
				}
				
		result.append(.Ctr(c, partresult))
	}
	return result
}


///////////////////////////////////////////////
//Find total instantiations of a partial value
///////////////////////////////////////////////

func total(_ val: Term) -> [Term]{
	switch val {
		case let .Var(p, sopSS):
			let ss = getSumOfProd(sopSS)
			var result : [Term] = []
			for x in new(p, ss){
				for y in total(x){
					result.append(y)
				}
			}
			return result
		
		case let .Ctr(c, xs):
		// mapM (x -> [x + 1]) [1,2,3] = [[2,3,4]]
		// This is how mapM works for lists.
			var ys : [Term] = []
			for i in xs{
				ys += total(i)
			}
			return [.Ctr(c, ys)]
	}

}

////////////////////////////
// Answers
////////////////////////////

func answer<A : SerialError, B>(_ a : A, _ known : ((A) -> B), _ unknown : ((Pos) -> B)) -> B{
	if let pos = a.errorPos {
		return unknown(pos)
	} else {
		return known(a)
	}
}


//////////////////////////////
// Refute
//////////////////////////////

func refute(_ r: Result) -> Int{
	let xs = r.args
	
	return ref(r, xs)
}

func ref(_ r: Result, _ xs : [Term]) -> Int{
	let known = { (b : Bool) -> Int in
		if b { return 1 }
		else { 
			print("Counter example found")
			var ys : [Term] = []
			for i in xs{
				ys += total(i)
			}
			
			if ys.isEmpty { print(", but to deep to fully instantiate") }
			else {
				print(":")
				let a = ys.head
				
			}
			
			return 0 }
	}
	
	let unknown = { (p) -> Int in
		sumMapM({ref(r, $0)}, 1, (refineList(xs, p)))
	}
	
	return eval( r.apply(xs), known, unknown)
}




func sumMapM<A>(_ f : ((A) -> Int), _ n : Int, _ bss : Array<A>) -> Int
{
	if bss.isEmpty {
		return n
	} else {
		let b = bss.head!
		let bs = bss.tail!
		let m = f(b)
		return sumMapM(f, (n+m), bs)
	}
}
////////////////////////////////
// Properties with parallel conjunction (Lindblad TFP'07)
////////////////////////////////
indirect enum Prop{
	case Bool(Bool)
	case Neg(Prop)
}

func eval<A>(_ p : Prop, _ k : (Bool) -> A, _ u : (Pos) -> A) -> A{

	return k(true)
}

////////////////////////////////
// Testable
////////////////////////////////

struct Result{
	let args : [Term]
	let showArgs : [(Term) -> String]
	let apply : ([Term]) -> Prop
}

struct Property{
	let P : (Int, Int) -> Result
}



///////////////////////////
//Helper functions for Swift
///////////////////////////
//This is an old version of the conv function. But swift is not lazy with the error evaluation. So this doesn't work.
enum UnevaluatedError : Error{
	case notYetEvaluated(Pos)
}

func conv_old<A>(_ cs : [([Term]) -> A], _ t : Term) throws -> A{
	switch t {
			
		case let .Var(p, _):
			throw UnevaluatedError.notYetEvaluated(p)
		
		case let .Ctr(i, xs):
			return cs[i](xs)
	}
	
}

extension Array{

	var head: Element?{
		return self.first
	}
	
	var tail: Array?{
		if self.isEmpty {
			return []
		}
		return Array(self.dropFirst())
	}
}

//Define the constant function. It takes two arguments, but always returns the first.
func const<A, B>(_ first : A) -> ((B) -> (A)){
	return { (b: B) -> A in
		first
	}
}

//Functions to unwrap our enumerations
func getSumOfProd(_ t : Type) -> [[Type]]{
	switch t {
			case let .SumOfProd(ts):
				return ts
			}
}

func getC<A>(_ x : Cons<A>) -> (Type, [([Term]) -> A]){
	switch x {
			case let .C(t, f):
				return (t, f)
			}
}

//Just like the haskell function splitAt
func splitAt<A>(_ n: Int, _ xs: Array<A>) -> (Array<A>, Array<A>){
	let length = xs.count
	var m = n
	if n > length {
		m = length }
	if n < 0 {
		m = 0 }
	
	return (Array(xs.dropLast(length - m)), Array(xs.dropFirst(m)))
}